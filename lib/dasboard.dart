import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:koperasi/auth.dart';
import 'package:stylish_bottom_bar/model/bar_items.dart';
import 'package:stylish_bottom_bar/stylish_bottom_bar.dart';

class DashboarPage extends StatefulWidget {
  const DashboarPage({super.key});

  @override
  State<DashboarPage> createState() => _DashboarPageState();
}

class _DashboarPageState extends State<DashboarPage> {
  dynamic selected = 0;
  var heart = false;
  PageController controller = PageController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text("Koperasi Undiksha"),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context)
                  .pushReplacement(MaterialPageRoute(builder: (context) {
                return LoginPage();
              }));
            },
            icon: Icon(Icons.logout),
            color: Colors.white,
          )
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(
                    left: 19, right: 5, top: 8, bottom: 8),
                width: width,
                height: 140,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(10)),
                child: Row(
                  children: [
                    ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: Image.asset(
                          "assets/images/user3.jpeg",
                          width: 100,
                          height: 100,
                        )),
                    const SizedBox(
                      width: 14,
                    ),
                    Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(top: 12.5),
                          padding: const EdgeInsets.symmetric(
                              vertical: 6, horizontal: 10),
                          width: 223,
                          height: 50,
                          decoration: BoxDecoration(
                              color: Color.fromARGB(255, 204, 208, 255),
                              borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Nasabah",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 12),
                              ),
                              Text(
                                "I Ketut Setiawan",
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        SingleChildScrollView(
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 6, horizontal: 10),
                            width: 223,
                            height: 50,
                            decoration: BoxDecoration(
                                color: Color.fromARGB(255, 204, 208, 255),
                                borderRadius: BorderRadius.circular(10)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Total Saldo Anda",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                ),
                                Text(
                                  "Rp. 12.000.000",
                                  style: TextStyle(fontSize: 15),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 25,
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 18, horizontal: 18),
                width: width,
                height: 260,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.blue,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(10)),
                child: GridView.count(
                  crossAxisCount: 3,
                  crossAxisSpacing: 16,
                  mainAxisSpacing: 16,
                  childAspectRatio: 4 / 4,
                  children: [
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 204, 208, 255),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.wallet,
                              color: Color.fromRGBO(8, 0, 253, 1),
                              size: 65,
                            ),
                            Text(
                              "Cek Saldo",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 204, 208, 255),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.money_outlined,
                              color: Color.fromRGBO(8, 0, 253, 1),
                              size: 65,
                            ),
                            Text(
                              "Transfer",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 204, 208, 255),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.monetization_on,
                              color: Color.fromRGBO(8, 0, 253, 1),
                              size: 65,
                            ),
                            Text(
                              "Deposito",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 204, 208, 255),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.credit_card_outlined,
                              color: Color.fromRGBO(8, 0, 253, 1),
                              size: 65,
                            ),
                            Text(
                              "Pembayaran",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 204, 208, 255),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.shopify_sharp,
                              color: Color.fromRGBO(8, 0, 253, 1),
                              size: 65,
                            ),
                            Text(
                              "Pinjaman",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 204, 208, 255),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.data_exploration_sharp,
                              color: Color.fromRGBO(8, 0, 253, 1),
                              size: 65,
                            ),
                            Text(
                              "Mutasi",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 25,
              ),
              Container(
                padding: const EdgeInsets.all(10),
                width: width,
                height: 80,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Butuh Bantuan ?",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "0878-1234-1024",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 34),
                        )
                      ],
                    ),
                    Icon(
                      Icons.phone,
                      color: Color.fromRGBO(8, 0, 253, 1),
                      size: 55,
                    ),
                  ],
                ),
                color: Color.fromARGB(255, 204, 208, 255),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: Container(
        width: width,
        height: 80,
        color: Color.fromARGB(255, 204, 208, 255),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.settings,
                  color: Color.fromRGBO(8, 0, 253, 1),
                  size: 60,
                ),
                Text(
                  "Setting",
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              ],
            ),
            Stack(
              children: [
                Positioned(
                  child: Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    width: 75,
                    height: 75,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(8, 0, 253, 1),
                        borderRadius: BorderRadius.circular(75 / 2)),
                    child: Image.asset(
                      "assets/images/qr.jpg",
                      width: 40,
                      height: 40,
                    ),
                  ),
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.person,
                  color: Color.fromRGBO(8, 0, 253, 1),
                  size: 60,
                ),
                Text(
                  "Profile",
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              ],
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

// launcher icon
// splash screen
// onboarding screen
// access native device (kamera,lokasi,ekternal storagae)
// qr scanner

// lotte  files
// story sate
// spline