import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:koperasi/model/User.dart';
import 'package:http/http.dart' as http;

final dio = Dio();

class KoperasiController {
  final String baseUrl = "http://apikoperasi.rey1024.com";

  Future<List<User>> getAllUser() async {
    final response = await dio.get(baseUrl + "/users");
    if (response.statusCode == 200) {
      final List<dynamic> jsonData = json.decode(response.data);
      return jsonData.map((json) => User.fromJson(json)).toList();
    } else {
      throw Exception('Failed to fetch products');
    }
  }

  Future getData() async {
    final response = await dio.get('http://apikoperasi.rey1024.com/users');
    try {
      return response;
    } catch (e) {
      return e;
    }
  }

  Future createUser(
      String nama, String password, String email, String nim) async {
    final dio = Dio();

    try {
      final response = await dio.post('http://apikoperasi.rey1024.com/register',
          data: {
            "nama": nama,
            "password": password,
            "username": email,
            "nim": nim,
          },
          options: Options(headers: {'Access-Control-Request-Method': 'POST'}));

      if (response.statusCode == 200) {
        // Berhasil melakukan POST request, Anda dapat mengelola respon di sini
        print(response.data);
      } else {
        // Gagal melakukan POST request, Anda dapat menangani kesalahan di sini
        print('Gagal melakukan POST request');
      }
    } catch (error) {
      // Tangani kesalahan lain yang mungkin terjadi selama proses
      print('Error: $error');
    }
  }
}
