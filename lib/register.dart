import 'package:flutter/material.dart';
import 'package:koperasi/api/koperasi.dart';
import 'package:koperasi/auth.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  String _password = '';
  late TextEditingController _passwordController;
  bool _showPassword = false;

  final _usernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _nimController = TextEditingController();

  KoperasiController _koperasi = new KoperasiController();

  @override
  void initState() {
    super.initState();
    _passwordController = TextEditingController();
  }

  @override
  void dispose() {
    _passwordController.dispose();
    super.dispose();
  }

  void togglePasswordVisibility() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          title: Text("Koperasi Undiksha"),
          centerTitle: true,
          backgroundColor: Colors.blue[900],
        ),
        body: SingleChildScrollView(
          child: Container(
            height: height * 1.1,
            child: Center(
              child: Column(
                children: [
                  SizedBox(
                      height: height * 0.25 + 5,
                      child: Image.asset(
                        "assets/images/logo.png",
                        width: 180,
                        height: 180,
                      )),
                  const SizedBox(
                    height: 4,
                  ),
                  Container(
                    width: width,
                    height: 540,
                    padding: const EdgeInsets.only(
                        top: 20, left: 8, right: 8, bottom: 5),
                    margin: const EdgeInsets.symmetric(horizontal: 5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Colors.blue,
                        width: 2,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Form(
                      key: _formKey,
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Text(
                                "Username",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.blue,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            Center(
                              child: Container(
                                width: 320,
                                child: TextFormField(
                                  controller: _usernameController,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'Nama pengguna tidak boleh kosong';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(6),
                                          borderSide: BorderSide(
                                              color: Colors.blue,
                                              width: 2,
                                              style: BorderStyle.solid))),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Text(
                                "NIM",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.blue,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            Center(
                              child: Container(
                                width: 320,
                                child: TextFormField(
                                  controller: _nimController,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'NIM tidak boleh kosong';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(6),
                                          borderSide: BorderSide(
                                              color: Colors.blue,
                                              width: 2,
                                              style: BorderStyle.solid))),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Text(
                                "Email",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.blue,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            Center(
                              child: Container(
                                width: 320,
                                child: TextFormField(
                                  controller: _emailController,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'Email tidak boleh kosong';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(6),
                                          borderSide: BorderSide(
                                              color: Colors.blue,
                                              width: 2,
                                              style: BorderStyle.solid))),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Text(
                                "Password",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.blue,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            Center(
                              child: Container(
                                width: 320,
                                child: TextFormField(
                                  controller: _passwordController,
                                  obscureText: !_showPassword,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'Password tidak boleh kosong';
                                    }
                                    return null;
                                  },
                                  onSaved: (value) {
                                    _password = value!;
                                  },
                                  decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                          onPressed: togglePasswordVisibility,
                                          icon: Icon(
                                            _showPassword
                                                ? Icons.visibility
                                                : Icons.visibility_off,
                                            color: Colors.grey,
                                          )),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(6),
                                          borderSide: BorderSide(
                                              color: Colors.blue,
                                              width: 2,
                                              style: BorderStyle.solid))),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Center(
                                child: Container(
                              width: 200,
                              height: 40,
                              decoration: BoxDecoration(boxShadow: [
                                BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(5, 4),
                                    blurRadius: 10,
                                    spreadRadius: 0)
                              ]),
                              child: FilledButton(
                                  style: ButtonStyle(
                                    padding: MaterialStatePropertyAll(
                                        EdgeInsets.symmetric(
                                            horizontal: 16, vertical: 8)),
                                    backgroundColor: MaterialStateProperty.all(
                                        Colors.blue[900]),
                                  ),
                                  onPressed: () async {
                                    if (_formKey.currentState!.validate()) {
                                      _formKey.currentState!.save();

                                      String name = _usernameController.text;
                                      String email = _emailController.text;
                                      String password =
                                          _passwordController.text;
                                      String nim = _nimController.text;
                                      await _koperasi.createUser(
                                          name, password, email, nim);
                                    }

                                    // Navigator.of(context).pushReplacement(
                                    //     MaterialPageRoute(builder: (context) {
                                    //   return LoginPage();
                                    // }));
                                  },
                                  child: Text("Register")),
                            )),
                            const SizedBox(
                              height: 18,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pushReplacement(
                                          MaterialPageRoute(builder: (context) {
                                        return LoginPage();
                                      }));
                                    },
                                    child: Text("Login Akun ",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue[900])),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 110),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 6, horizontal: 8),
                          alignment: Alignment.center,
                          width: width,
                          height: 50,
                          color: Colors.blue[200],
                          child: Text(
                            "copyright@2022 by Undiksha",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
